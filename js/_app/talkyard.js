
var _talkyard = {
    load: function (sguid, term) {
        $('#ty-comments').empty();
        var discussion_id = 'translations/' + $config.lng + '/' + sguid
        $('#ty-comments').attr('data-discussion-id', discussion_id);
        $('#ty-script').remove();
        var src = $config.talkyard_url + '/-/talkyard-comments.min.js';
        $('head').append('<script id="ty-script" async type="text/javascript" src="' + src + '"><\/script>');
    },
};
